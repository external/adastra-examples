#!/bin/bash

set -e

source "cray_environment.sh"

cc -std=c11 -fopenmp -Wall -Wextra -pedantic -O3 hello_cpu_binding.c -o hello_cpu_binding_cray
