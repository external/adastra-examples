#!/bin/bash

set -e

source "cray_environment.sh"

CC -std=c++11 -fopenmp -x hip -Wall -Wextra -pedantic -O3 -L${ROCM_PATH}/lib -lamdhip64 hello_gpu_binding.cc -o hello_gpu_binding_cray
