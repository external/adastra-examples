# Adastra examples

This git repository contains some basic test codes which the users of Adastra may find useful for testing the Adastra environment and the functioning of GPUs.

Please don't hesitate to suggest us other such codes which we may keep here for everyone.
