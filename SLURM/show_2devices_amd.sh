#!/bin/bash

sleep 3
# Note : Les premiers devices sont les CPUs, nous affichons donc les deux derniers devices
echo "$(hostname) [$(date)]: $(rocminfo | grep -i uuid | tail -n 2 | xargs)"
rocm-smi
