#!/bin/bash

set -eu

################################################################################
# Machine configurations
################################################################################

########################################
# For Adastra's GPU partition partition
########################################

function Adastra_MI250_8TasksWith8ThreadsAnd1GPU() {
    # Node local rank 0 gets the GCD 0, is bound the cores [48-55] of NUMA domain 3 and uses the NIC 0
    # Node local rank 1 gets the GCD 1, is bound the cores [56-63] of NUMA domain 3 and uses the NIC 0
    # Node local rank 2 gets the GCD 2, is bound the cores [16-23] of NUMA domain 1 and uses the NIC 1
    # Node local rank 3 gets the GCD 3, is bound the cores [24-31] of NUMA domain 1 and uses the NIC 1
    # Node local rank 4 gets the GCD 4, is bound the cores [ 0- 7] of NUMA domain 0 and uses the NIC 2
    # Node local rank 5 gets the GCD 5, is bound the cores [ 8-15] of NUMA domain 0 and uses the NIC 2
    # Node local rank 6 gets the GCD 6, is bound the cores [32-39] of NUMA domain 2 and uses the NIC 3
    # Node local rank 7 gets the GCD 7, is bound the cores [40-47] of NUMA domain 2 and uses the NIC 3
    AFFINITY_NUMACTL=('48-55' '56-63' '16-23' '24-31' '0-7' '8-15' '32-39' '40-47')
    AFFINITY_GPU=('0' '1' '2' '3' '4' '5' '6' '7')
    export MPICH_OFI_NIC_POLICY=NUMA
}

function Adastra_MI250_8TasksWith16ThreadsAnd1GPU() {
    # Requires SMT to be enabled.
    AFFINITY_NUMACTL=('48-55,112-119' '56-63,120-127' '16-23,80-87' '24-31,88-95' '0-7,64-71' '8-15,72,79' '32-39,96-103' '40-47,104-111')
    AFFINITY_GPU=('0' '1' '2' '3' '4' '5' '6' '7')
    export MPICH_OFI_NIC_POLICY=NUMA
}

function Adastra_MI250_16TasksWith8ThreadsAnd1GPU() {
    # Requires SMT to be enabled.
    AFFINITY_NUMACTL=('48-51,112-115' '52-55,116-119' '56-59,120-123' '60-63,124-127' '16-19,80-83' '20-23,84-87' '24-27,88-91' '28-31,92-95' '0-3,64-67' '4-7,68-71' '8-11,72-75' '12-15,76-79' '32-35,96-99' '36-39,100-103' '40-43,104-107' '44-47,108-111')
    AFFINITY_GPU=('0' '0' '1' '1' '2' '2' '3' '3' '4' '4' '5' '5' '6' '6' '7' '7')
    export MPICH_OFI_NIC_POLICY=NUMA
}

########################################
# For Adastra's CPU partition partition
########################################

function Adastra_GENOA_8TasksWith6ThreadsSparseOnL3() {
    AFFINITY_NUMACTL=('0-1,8-9,16-17' '24-25,32-33,40-41' '48-49,56-57,64-65' '72-73,80-81,88-89' '96-97,104-105,112-113' '120-121,128-129,136-137' '144-145,152-153,160-161' '168-169,176-177,184-185')
}

function Adastra_GENOA_8TasksWith6ThreadsDenseOnL3() {
    AFFINITY_NUMACTL=('0-5' '24-29' '48-53' '72-77' '96-101' '120-125' '144-149' '168-173')
}

function Adastra_GENOA_8TasksWith24Threads() {
    AFFINITY_NUMACTL=('0-23' '24-47' '48-71' '72-95' '96-119' '120-143' '144-167' '168-191')
}

function Adastra_GENOA_16TasksWith12Threads() {
    AFFINITY_NUMACTL=('0-11' '12-23' '24-35' '36-47' '48-59' '60-71' '72-83' '84-95' '96-107' '108-119' '120-131' '132-143' '144-155' '156-167' '168-179' '180-191')
}

function Adastra_GENOA_24TasksWith8Threads() {
    AFFINITY_NUMACTL=('0-7' '8-15' '16-23' '24-31' '32-39' '40-47' '48-55' '56-63' '64-71' '72-79' '80-87' '88-95' '96-103' '104-111' '112-119' '120-127' '128-135' '136-143' '144-151' '152-159' '160-167' '168-175' '176-183' '184-191')
}

function Adastra_GENOA_24TasksWith16Threads() {
    # Requires SMT to be enabled.
    AFFINITY_NUMACTL=('0-7,192-199' '8-15,200-207' '16-23,208-215' '24-31,216-223' '32-39,224-231' '40-47,232-239' '48-55,240-247' '56-63,248-255' '64-71,256-263' '72-79,264-271' '80-87,272-279' '88-95,280-287' '96-103,288-295' '104-111,296-303' '112-119,304-311' '120-127,312-319' '128-135,320-327' '136-143,328-335' '144-151,336-343' '152-159,344-351' '160-167,352-359' '168-175,360-367' '176-183,368-375' '184-191,376-383')
}

function Adastra_GENOA_32TasksWith4Threads() {
    AFFINITY_NUMACTL=('0-3' '4-7' '8-11' '16-19' '24-27' '28-31' '32-35' '40-43' '48-51' '52-55' '56-59' '64-67' '72-75' '76-79' '80-83' '88-91' '96-99' '100-103' '104-107' '112-115' '120-123' '124-127' '128-131' '136-139' '144-147' '148-151' '152-155' '160-163' '168-171' '172-175' '176-179' '184-187')
}

function Adastra_GENOA_32TasksWith6Threads() {
    AFFINITY_NUMACTL=('0-5' '6-11' '12-17' '18-23' '24-29' '30-35' '36-41' '42-47' '48-53' '54-59' '60-65' '66-71' '72-77' '78-83' '84-89' '90-95' '96-101' '102-107' '108-113' '114-119' '120-125' '126-131' '132-137' '138-143' '144-149' '150-155' '156-161' '162-167' '168-173' '174-179' '180-185' '186-191')
}

function Adastra_GENOA_48TasksWith4Threads() {
    AFFINITY_NUMACTL=('0-3' '4-7' '8-11' '12-15' '16-19' '20-23' '24-27' '28-31' '32-35' '36-39' '40-43' '44-47' '48-51' '52-55' '56-59' '60-63' '64-67' '68-71' '72-75' '76-79' '80-83' '84-87' '88-91' '92-95' '96-99' '100-103' '104-107' '108-111' '112-115' '116-119' '120-123' '124-127' '128-131' '132-135' '136-139' '140-143' '144-147' '148-151' '152-155' '156-159' '160-163' '164-167' '168-171' '172-175' '176-179' '180-183' '184-187' '188-191')
}

################################################################################
# GPU binding
################################################################################

function GPUAffinityAMD() {
    export ROCR_VISIBLE_DEVICES="${AFFINITY_GPU[${1}]}"
}

################################################################################

# Define your machine configuration:
Adastra_MI250_8TasksWith8ThreadsAnd1GPU
# Adastra_MI250_8TasksWith16ThreadsAnd1GPU
# Adastra_MI250_16TasksWith8ThreadsAnd1GPU

# Adastra_GENOA_8TasksWith6ThreadsSparseOnL3
# Adastra_GENOA_8TasksWith6ThreadsDenseOnL3
# Adastra_GENOA_8TasksWith24Threads
# Adastra_GENOA_16TasksWith12Threads
# Adastra_GENOA_24TasksWith8Threads
# Adastra_GENOA_32TasksWith4Threads
# Adastra_GENOA_32TasksWith6Threads
# Adastra_GENOA_48TasksWith4Threads

# LOCAL_RANK: In [0, SLURM_TASKS_PER_NODE), uniquely identifies the tasks of a
# node. Under SLURM, we use SLURM_LOCALID.
LOCAL_RANK=$((${SLURM_LOCALID} % ${#AFFINITY_NUMACTL[@]}))

# Define your GPU binding (or comment if not needed):
GPUAffinityAMD ${LOCAL_RANK}

################################################################################

echo "Starting local rank: ${LOCAL_RANK} with: 'numactl --localalloc --physcpubind=${AFFINITY_NUMACTL[${LOCAL_RANK}]} --'"

# NOTE: here, we use numactl but OpenMP, taskset or hw-bind are alternatives.
exec numactl --localalloc --physcpubind=${AFFINITY_NUMACTL[${LOCAL_RANK}]} -- "${@}"
